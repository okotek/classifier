module gitlab.com/okotek/classifier

go 1.17

require gocv.io/x/gocv v0.30.0

require (
	gitlab.com/okotek/classifyFunctions v0.0.0-00010101000000-000000000000
	gitlab.com/okotek/loadoptions v0.0.0-00010101000000-000000000000
	gitlab.com/okotek/okonet v0.0.0-20220111050325-7dff580a79d7
	gitlab.com/okotek/okotypes v0.0.0-20220328184655-40ed17237fbd
)

replace gitlab.com/okotek/classifyFunctions => ../classifyFunctions

replace gitlab.com/okotek/loadoptions => ../loadoptions

replace gitlab.com/okotek/okonet => ../okonet

replace gitlab.com/okotek/okotypes => ../okotypes

replace gitlab.com/okotech/classifyFunctions => ../classifyFunctions
