/*
	Documentation and explanation right here:

	This was previously a program that would use multiple classifiers, but in the name of simplicity, fexibility and
	stability, this feature has been removed in favor of running multiple processes instead of spawining many goroutines.

*/
/*
	Random naming of each frame has been put on the back burner for now. In searching for
	the cause of an error where classifier locks up, each image has been given the exact
	same string as a name until this can be fixed.
*/
package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"

	//"math/rand"
	"os"
	//"strings"
	"time"
	//"image"

	//classifyFunctions "../classifierFunctions/classifyFunctions"
	//"../classifierFunctions/classifyFunctions.go"
	classifyFunctions "gitlab.com/okotek/classifyFunctions"

	//loadOptions "../loadOptions"
	//okolog "../okolog"
	loadOptions "gitlab.com/okotek/loadoptions"
	okonet "gitlab.com/okotek/okonet"
	types "gitlab.com/okotek/okotypes"

	"gocv.io/x/gocv"
)

var cfig types.ConfigOptions

func main() {
	cfig = loadOptions.Load()
	//okolog.LoggerIDInit(cfig)
	//cfig.InstanceIdentifier = okolog.InitStruct.Identifier

	sendoffTarget := flag.String("sendoffTarget", "localhost:8082", "Target to send classified frames to.")
	listenTarget := flag.String("listenTarget", ":8081", "Port to listen on.")
	classifierLocation := flag.String("classifierLocation", "./classifiers/classifier.xml", "Location of classifier file for this process to use.")

	flag.Parse()

	//If the cli location is changed from default, go with that, else, go with the cofig file version. If neither of these conditions exist,
	//use the default flag value.
	if *classifierLocation != "./classifiers/classifier.xml" {
		if len(cfig.ClassifierXMLLocation) > 1 {
			*classifierLocation = cfig.ClassifierXMLLocation
		}
	}

	stageOneChan := make(chan types.Frame)
	stageTwoChan := make(chan types.Frame)
	finChan := make(chan types.Frame)
	randChan := make(chan types.Frame)
	positionChan := make(chan types.PositionAndIdentifier)
	positionChanReturn := make(chan bool)

	/*
		go func() {
			//Explain this.
			time.Sleep(time.Second * 4)
			for {
				tmp := <-stageTwoChan
				if len(tmp.ClassifierTags) > 0 {
					//okolog.MessageLogger("Length of classifier tags is nonzero. Sending.")
					randChan <- tmp
				} else {
					//okolog.MessageLogger("Length of classifier tags is zero. Continuing.")
					continue
				}
			}
		}()
	*/

	//Skipping that function.
	go func() {
		var counter int64 = 0

		for {
			tmp := <-stageTwoChan
			counter++
			if counter%15 == 0 {
				fmt.Println(counter)
			}
			randChan <- tmp
		}

	}()

	go generateRandomString(randChan, finChan)
	go overDetectionFeedback(positionChan, positionChanReturn)
	go okonet.SendoffHandlerCached(finChan, *sendoffTarget, 25)
	go okonet.ReceiverHandler(stageOneChan, *listenTarget)
	//go okonet.ReceiverHandler(stageOneChan, ":9051")      <-- What is this for?

	go classify(stageOneChan, stageTwoChan, *classifierLocation, 0, positionChan, positionChanReturn)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

}

func classify(input chan types.Frame, output chan types.Frame, target string, stageNumber int, positionChan chan types.PositionAndIdentifier, positionChanReturn chan bool) {

	//go okolog.MessageLogger("Starting new classification check.")

	clsfr := gocv.NewCascadeClassifier()
	defer clsfr.Close()

	if !clsfr.Load(target) {
		log.Printf("Error reading cascade file: %v\n", target)
		return
	}

	log.Printf("\nLoaded cascade file. <<<<==========<<<<\n\n")

	cfig.ClassifierMode = "SingleThread"

	if cfig.ClassifierMode == "SingleThread" {
		classifyFunctions.SerialClassify(input, output, clsfr, target)
	} else {
		classifyFunctions.SerialClassify(input, output, clsfr, target)
	}
}

func generateRandomString(input chan types.Frame, output chan types.Frame) {

	var runes = []rune{'a', 'b', 'c', '.', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', '_', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}
	var runeString string = ""

	for iter := range input {
		const min = 0
		var max = len(runes)

		for i := 0; i < 15; i++ {
			randnumb := rand.Intn(max-min) + min
			randRune := runes[randnumb]
			runeString = fmt.Sprintf("%s%c", runeString, randRune)
		}

		iter.ImgName = runeString
		runeString = ""
		output <- iter
	}
}

/*
	In the future, this should be optimized so that it isn't holding
	back other routines
*/
/*
func _generateRandomString(input chan types.Frame, output chan types.Frame) {

	tokens := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!#$&()*"
	characters := strings.Split(tokens, "")

	var outString [25]string
	var stringStorePt []string
	var stringStore *[]string = &stringStorePt
	var tmp string = "" // for assigning to frame
	tmpChan := make(chan string)

	//Create and maintain a buffer of random strings to use as image names

	go func() {
		for {
			if len(*stringStore) < 150 {
				for iter := 0; iter < len(outString); iter++ {
					outString[iter] = characters[rand.Intn(len(tokens)-2)]
				}
				asdf := strings.Join(outString[:], "")
				*stringStore = append(*stringStore, asdf)
			} else {
				continue
			}

			//okolog.MessageLogger(fmt.Sprintf("StringStore length: %d\n", len(*stringStore)))
		}
	}()

	/*
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		WARNING: I've been getting random "slice bounds out of range [:-1]" panics here.
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
/*
	go func() {
		for {
			tmpString := *stringStore
			if len(tmpString) < 1 {
				go okolog.MessageLogger("tmpString is length zero in trying to create a random string.")
			}
			tmp, tmpString = tmpString[len(tmpString)-1], tmpString[:len(tmpString)-1]
			*stringStore = tmpString
			tmpChan <- tmp
		}
	}()

	go func() {

		var tmp string
		for iter := range input {
			tmp = <-tmpChan
			//okolog.MessageLogger("Writing random string to frame: " + tmp)
			iter.ImgName = tmp
			output <- iter
		}
	}()

}
*/
/*
	overDetectionFeedback takes an input that represents classifier detections
	and runs over them to check for over-detections. If everything checks out,
	a True bool is sent back out. Otherwise, a False is sent.

	Detection count tuning is accomplished with the tuningModifier variable. The number
	represents the number of hours since the detection came in.
*/
func overDetectionFeedback(input chan types.PositionAndIdentifier, positionChanReturn chan bool) {

	/*
		tmpID is an identifier for detections. idMap stores all of these.
		Care should be taken considering memory usage.
	*/
	//tmpID := types.PositionMatrix{0.0, time.Now()}
	idMap := make(map[types.PositionAndIdentifier]types.PositionMatrix)
	tuningModifier := 1.0
	goNoGoThreshold := 0.1

	/*
		Mark places in a frame where detections have happened before and
		then fade them when it's been awhile since.
	*/
	for iter := range input {
		//go okolog.MessageLogger("OverDetectionFeedback has received a new dataset.")

		if _, ok := idMap[iter]; ok {
			//go okolog.MessageLogger("idMap was good.")

			idMap[iter] = getPositionScore(tuningModifier, idMap[iter])

			if idMap[iter].DetectionFactor > goNoGoThreshold {
				positionChanReturn <- true
			} else {
				positionChanReturn <- false
			}
		} else {
			//go okolog.MessageLogger("Creating new idMat element.")
			//tmp := types.PositionMatrix{1, time.Now()}
			idMap[iter] = types.PositionMatrix{1, time.Now()}
			positionChanReturn <- true
		}

	}
}

/*
	The position scores are scores that get applied to certain points in a
	single camera's image. For instance,if the pixel at 400x550 gives a positive detection
	every frame, the score for 400x550 would skyrocket. If, after awhile, the detections
	go away, the score would slowly decrease.
*/
func getPositionScore(mod float64, posMat types.PositionMatrix) types.PositionMatrix {
	tmpDetFac := posMat.DetectionFactor
	diff := time.Now().Sub(posMat.LastDetectionTime).Hours()

	//Modify the detectionFactor variable so that beyond "mod" hours ago, the factor decreases,
	//with the inverse also being true.
	if diff < mod {
		tmpDetFac = tmpDetFac * (diff * 2)
	} else {
		tmpDetFac = tmpDetFac / (diff * 2)
	}

	//Simple CSV file to store the position scores
	logFile, er := os.OpenFile("./logs/positionScoreLog.csv", os.O_APPEND|os.O_WRONLY, 0600)
	if er != nil {
		fmt.Println(er)
	}
	defer logFile.Close()

	dump := []byte(fmt.Sprintf("%f", diff))
	dump = append(dump, byte('\n'))
	logFile.Write(dump)

	posMat.LastDetectionTime = time.Now()
	posMat.DetectionFactor = tmpDetFac

	return posMat
}

/*
func _classify(input chan types.Frame, output chan types.Frame, target string) {

	clsfr := gocv.NewCascadeClassifier()
	defer clsfr.Close()

	if !clsfr.Load(target) {
		log.Printf("Error reading cascade file: %v\n", target)
		return
	}

	log.Printf("\nLoaded cascade file. <<<<==========<<<<\n\n")

	cfig.ClassifierMode = "SingleThread"

	if cfig.ClassifierMode == "SingleThread" {
		classifyFunctions.SerialClassify(input, output, clsfr)
	} else {
		classifyFunctions.SerialClassify(input, output, clsfr)
	}

}
*/
